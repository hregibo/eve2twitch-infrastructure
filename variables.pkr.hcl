variable "registry_uri" {
  type    = string
  default = "hub.docker.com"
}

variable "registry_username" {
  type    = string
  default = "administrator"
}

variable "registry_password" {
  type = string
}

variable "image_version" {
  type = string
}
