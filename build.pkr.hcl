locals {
  project = "eve2twitch"

}

build {
  name = "${var.registry_uri}/${local.project}/frontend-test"
  sources = [
    "source.docker.debian"
  ]
  provisioner "shell" {
    script = "./scripts/update-repositories.sh"
  }
  provisioner "shell" {
    script = "./scripts/base.sh"
  }
  provisioner "shell" {
    script = "./scripts/cloud-init.sh"
  }
  post-processors {
    post-processor "docker-tag" {
      repository = build.name
      tags = ["latest", var.image_version]
    }
    post-processor "docker-push" {
      login = true
      login_username = var.registry_username
      login_password = var.registry_password
      login_server = var.registry_uri
    }
  }
}
