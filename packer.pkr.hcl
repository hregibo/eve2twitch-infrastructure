packer {
  required_plugins {
    docker = {
      version = " >= 1.0.0 "
      source  = "github.com/hashicorp/docker"
    }
  }
}
