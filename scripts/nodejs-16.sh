#!/usr/bin/env bash

NODE_VERSION="v16.13.2"
FOLDER_NAME="node-$NODE_VERSION-linux-x64"

printf "Downloading NodeJS %s" $NODE_VERSION
curl -o $FOLDER_NAME.tar.xz https://nodejs.org/dist/v16.13.2/$FOLDER_NAME.tar.xz
printf "Decompressing NodeJS archive into /tmp"
tar -C /tmp -xvf $FOLDER_NAME.tar.xz
printf "Installing NodeJS binaries into their respective folders"
cp -r /tmp/$FOLDER_NAME/{bin,include,lib,share} /usr/
